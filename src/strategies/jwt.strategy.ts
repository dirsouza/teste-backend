import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AuthRepository } from '../repository';
import { JwtPayloadInterface } from '../interfaces';
import { User } from '../entities';
import * as config from 'config';

const jwtConfig = config.get('jwt');

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(AuthRepository)
    private authRepository: AuthRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET || jwtConfig.secret,
    });
  }

  async validate(jwtPayload: JwtPayloadInterface): Promise<User> {
    const { id } = jwtPayload;
    const user = await this.authRepository.findOne(id, {
      select: ['id', 'username', 'profile'],
    });

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
