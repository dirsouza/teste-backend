import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { Profile } from '../enums';

export class ProfileAdminDto {
  @IsOptional()
  @IsEnum(Profile)
  @ApiProperty({
    enum: Profile,
    example: Profile.ADMIN,
  })
  profile: Profile;
}
