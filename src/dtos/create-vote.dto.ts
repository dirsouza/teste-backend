import { ApiProperty, OmitType } from '@nestjs/swagger';
import { VoteDto } from './vote.dto';

export class CreateVoteDto extends OmitType(VoteDto, ['id', 'movie', 'user', 'createdAt'] as const) {
  @ApiProperty({
    type: Number,
    example: 1,
  })
  movie_id: 1;
}
