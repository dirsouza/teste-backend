import { ApiProperty } from '@nestjs/swagger';
import { AccessTokenInterface } from '../interfaces';

export class AccessTokenDto implements AccessTokenInterface {
  @ApiProperty({
    type: String,
    example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ'
  })
  token: string;

  @ApiProperty({
    type: Number,
    example: 3600,
  })
  expires_in: number;

  @ApiProperty({
    type: String,
    example: 'Bearer',
  })
  type: string;
}
