import { ApiProperty } from '@nestjs/swagger';
import { MovieDto } from './movie.dto';

export class DetailsMovieDto extends MovieDto {
  @ApiProperty({
    type: String,
    example: '4.4',
  })
  averageRating: number;
}
