import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsString, Matches, MaxLength, MinLength } from 'class-validator';
import { Profile } from '../enums';

export class UserDto {
  @ApiProperty({
    example: 1
  })
  id: number;

  @IsNotEmpty()
  @IsString()
  @MinLength(4)
  @MaxLength(40)
  @ApiProperty({
    type: String,
    example: 'joao.silva'
  })
  username: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  @Matches(/(?:(?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  @ApiProperty({
    type: String,
    example: 'Abc@123'
  })
  password: string;

  @IsOptional()
  @IsEnum(Profile)
  @ApiProperty({
    enum: Profile,
    example: Profile.USER,
  })
  profile: Profile;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  @ApiProperty()
  deletedAt: Date;
}
