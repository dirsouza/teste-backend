import { IntersectionType, OmitType } from '@nestjs/swagger';
import { UserDto } from './user.dto';
import { ProfileAdminDto } from './profile-admin.dto';

export class AdminDto extends IntersectionType(
  OmitType(UserDto, ['profile'] as const),
  ProfileAdminDto,
) {}
