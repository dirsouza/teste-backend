import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsPositive, Max, Min } from 'class-validator';
import { Movie, User } from '../entities';

export class VoteDto {
  @ApiProperty({
    example: 1,
  })
  id: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(4)
  @ApiProperty({
    type: Number,
    example: 4,
  })
  rating: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  movie: Movie;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  user: User;

  @ApiProperty()
  createdAt: Date;
}
