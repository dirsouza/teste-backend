import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsString } from 'class-validator';

export class MovieDto {
  @ApiProperty({
    example: 1,
  })
  id: number;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    type: String,
    example: 'Avengers: Endgame',
  })
  name: string;

  @IsNotEmpty()
  @IsArray()
  @ApiProperty({
    type: [String],
    example: ['Action', 'Adventure', 'Drama'],
  })
  genre: string[];

  @IsNotEmpty()
  @IsArray()
  @ApiProperty({
    type: [String],
    example: ['Joe Russo', 'Anthony Russo'],
  })
  director: string[];

  @IsNotEmpty()
  @IsArray()
  @ApiProperty({
    type: [String],
    example: ['Robert Downey Jr.', 'Chris Evans'],
  })
  actors: string[];

  @ApiProperty()
  createdAt: Date;
}
