import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class FilterMovieDto {
  @IsOptional()
  @IsNotEmpty()
  @ApiPropertyOptional({
    type: String,
    example: 'Avengers: Endgame'
  })
  search: string;
}
