import { CreateUserDto } from './create-user.dto';

export class CredentialAuthDto extends CreateUserDto {}
