import * as bcrypt from 'bcrypt';

export class BcryptUtils {
  static async hashPassword(pwd: string): Promise<string> {
    const salt = await this.generateSalt();
    return await bcrypt.hash(pwd, salt);
  }

  static async hashValidate(pwd: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(pwd, hash);
  }

  private static async generateSalt(): Promise<string> {
    return await bcrypt.genSalt();
  }
}
