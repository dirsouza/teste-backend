import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';
import * as config from 'config';

async function bootstrap() {
  const srvConfg = config.get('server');
  const swgConfg = config.get('swagger');
  const logger = new Logger('bootstrap');

  const app = await NestFactory.create(AppModule);

  /* Service */
  const {
    host,
    port,
    prefix: srvPrefix,
  } = srvConfg;

  app.setGlobalPrefix(srvPrefix);
  app.use(bodyParser.json({limit: '10mb'}));
  app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));
  app.enableCors();

  /* Swagger */
  const {
    title,
    description,
    version,
    prefix: swgPrefix,
  } = swgConfg;

  const swgOptions = new DocumentBuilder()
    .setTitle(process.env.SWG_TITLE || title)
    .setDescription(process.env.SWG_DESCRIPTION || description)
    .setVersion(process.env.SWG_VERSION || version)
    .addBearerAuth()
    .build();

  const swgDocument = SwaggerModule.createDocument(app, swgOptions);
  SwaggerModule.setup(swgPrefix, app, swgDocument);

  await app.listen(port);

  logger.debug(`Appliation listening on: http://${ host }:${ port }/${ srvPrefix }`);
  logger.debug(`Document API is running on: http://${ host }:${ port }/${ swgPrefix }`);
}

bootstrap();
