import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../repository';
import { CreateUserDto, UpdateUserDto } from '../dtos';
import { User } from '../entities';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async findUserById(id: number): Promise<User> {
    const user = await this.userRepository.findUserById(id);

    if (!user) {
      throw new NotFoundException(`User with ID '${ id }' not found`);
    }

    return user;
  }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    return await this.userRepository.createUser(createUserDto);
  }

  async updateUser(id: number, updateUserDto: UpdateUserDto): Promise<boolean> {
    const user = await this.findUserById(id);

    const result = await this.userRepository.updateUser(user, updateUserDto);

    return !(result.affected === 0);
  }

  async deleteUser(id: number): Promise<boolean> {
    const user = await this.findUserById(id);

    const result = await this.userRepository.deleteUser(user);

    return !(result.affected === 0);
  }
}
