import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { AuthRepository, UserRepository } from '../repository';
import { CredentialAuthDto } from '../dtos';
import { AccessTokenInterface, JwtPayloadInterface } from '../interfaces';
import * as config from 'config';
import { Profile } from '../enums';

@Injectable()
export class AuthService {
  private readonly jwtConfig;

  constructor(
    @InjectRepository(AuthRepository)
    private authRepository: AuthRepository,
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {
    this.jwtConfig = config.get('jwt');
  }

  async signIn(credentialAuthDto: CredentialAuthDto): Promise<AccessTokenInterface> {
    const user = await this.authRepository.signIn(credentialAuthDto);

    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const { id, username, profile } = user;

    return await this.sign({ id, username, profile });
  }

  private async sign(jwtPayload: JwtPayloadInterface): Promise<AccessTokenInterface> {
    return {
      token: await this.jwtService.sign(jwtPayload),
      expires_in: this.jwtConfig.expiresIn,
      type: this.jwtConfig.type,
    }
  }
}
