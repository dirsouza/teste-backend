import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '../repository';
import { CreateAdminDto, UpdateAdminDto } from '../dtos';
import { User } from '../entities';
import { Profile } from '../enums';

@Injectable()
export class AdminsService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async findAdminById(id: number): Promise<User> {
    const admin = await this.userRepository.findUserById(id, Profile.ADMIN);

    if (!admin) {
      throw new NotFoundException(`Admin with ID '${ id }' not found`);
    }

    return admin;
  }

  async createAdmin(createAdminDto: CreateAdminDto): Promise<User> {
    return await this.userRepository.createUser(createAdminDto, Profile.ADMIN);
  }

  async updateAdmin(id: number, updateAdminDto: UpdateAdminDto): Promise<boolean> {
    const admin = await this.findAdminById(id);

    const result = await this.userRepository.updateUser(admin, updateAdminDto);

    return !(result.affected === 0);
  }

  async deleteAdmin(id: number): Promise<boolean> {
    const admin = await this.findAdminById(id);

    const result = await this.userRepository.deleteUser(admin);

    return !(result.affected === 0);
  }
}
