import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MovieRepository, VoteRepository } from '../repository';
import { CreateMovieDto, CreateVoteDto, DetailsMovieDto, FilterMovieDto, MovieDto } from '../dtos';
import { Movie, User, Vote } from '../entities';
import { Profile } from '../enums';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(MovieRepository)
    private movieRepository: MovieRepository,
    @InjectRepository(VoteRepository)
    private voteRepository: VoteRepository,
  ) {}

  async listMovies(filterMovieDto: FilterMovieDto): Promise<MovieDto[]> {
    const movies = await this.movieRepository.findMovies(filterMovieDto);

    return movies.map(movie => this.preperMovieDto(movie));
  }

  async detailsMovie(id: number): Promise<DetailsMovieDto> {
    const movie = await this.movieRepository.findMovieById(id);

    if (!movie) {
      throw new NotFoundException(`Movie with ID '${ id }' not found`);
    }

    Object.assign(movie, this.preperMovieDto(movie));

    const { avgRating } = await this.voteRepository.getAvgRating(movie.id);

    return {
      ...movie,
      averageRating: avgRating ? avgRating : '0.0',
    } as unknown as DetailsMovieDto;
  }

  async createMovie(createMovieDto: CreateMovieDto, user: User): Promise<MovieDto> {
    if (user.profile !== Profile.ADMIN) {
      throw new UnauthorizedException('User is not an administrator');
    }

    const movie = await this.movieRepository.createMovie(createMovieDto);

    return this.preperMovieDto(movie);
  }

  async createVote(createVoteDto: CreateVoteDto, user: User): Promise<Vote> {
    if (user.profile !== Profile.USER) {
      throw new UnauthorizedException('User is administrator');
    }

    const movie = await this.movieRepository.findMovieById(createVoteDto.movie_id);

    return this.voteRepository.createVote(createVoteDto, movie, user);
  }

  private preperMovieDto(movie: Movie): MovieDto {
    return {
      ...movie,
      genre: movie.genre.split(','),
      director: movie.director.split(','),
      actors: movie.actors.split(','),
    } as MovieDto;
  }
}
