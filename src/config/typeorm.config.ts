import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

const dbConfig = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: process.env.DB_CONNECTION || dbConfig.connection,
  host: process.env.DB_HOST || dbConfig.host,
  port: process.env.DB_PORT || dbConfig.port,
  username: process.env.DB_USERNAME || dbConfig.username,
  password: process.env.DB_PASSWORD || dbConfig.password,
  database: process.env.DB_DATABASE || dbConfig.database,
  charset: process.env.DB_CHARSET || dbConfig.charset,
  synchronize: process.env.DB_SYNCHRONIZE || dbConfig.synchronize,
  logging: process.env.DB_LOGGING || dbConfig.logging,
  entities: [__dirname + '/../entities/*.entity.{js,ts}'],
}
