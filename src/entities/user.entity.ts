import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { BcryptUtils } from '../utils';
import { Profile } from '../enums';
import { Vote } from '.';

@Entity('users')
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', {
    unique: true,
    length: 40,
  })
  username: string;

  @Column('varchar', {
    select: false,
  })
  password: string;

  @Column('enum', {
    enum: Profile,
    default: Profile.USER,
    select: false,
  })
  profile: Profile

  @OneToMany(
    type => Vote,
    vote => vote.user
  )
  votes: Vote[];

  @CreateDateColumn({
    type: 'datetime',
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
    name: 'updated_at',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'datetime',
    name: 'deleted_at',
  })
  deletedAt: Date;

  async passwordValidade(pwd: string) : Promise<boolean> {
    return await BcryptUtils.hashValidate(pwd, this.password);
  }
}
