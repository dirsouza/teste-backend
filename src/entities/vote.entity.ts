import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn, ManyToOne, JoinColumn,
} from 'typeorm';
import { Movie, User } from '.';

@Entity('votes')
export class Vote extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('integer', {
    default: 0,
  })
  rating: number;

  @ManyToOne(type => Movie)
  @JoinColumn({
    name: 'movie_id',
    referencedColumnName: 'id',
  })
  movie: Movie;

  @ManyToOne(type => User)
  @JoinColumn({
    name: 'user_id',
    referencedColumnName: 'id',
  })
  user: User;

  @CreateDateColumn({
    type: 'datetime',
    name: 'created_at',
  })
  createdAt: Date;
}
