import {
  Entity,
  BaseEntity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn, OneToMany,
} from 'typeorm';
import { Vote } from '.';

@Entity('movies')
export class Movie extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', {
    unique: true,
  })
  name: string;

  @Column()
  genre: string;

  @Column()
  director: string;

  @Column()
  actors: string;

  @OneToMany(
    type => Vote,
      vote => vote.movie
  )
  votes: Vote[];

  @CreateDateColumn({
    type: 'datetime',
    name: 'created_at',
  })
  createdAt: Date;
}
