import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config';
import { AdminModule, AuthModule, MovieModule, UserModule } from './modules';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthModule,
    AdminModule,
    UserModule,
    MovieModule,
  ],
})
export class AppModule {
}
