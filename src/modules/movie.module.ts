import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovieRepository, VoteRepository } from '../repository';
import { AuthModule } from './auth.module';
import { MoviesController } from '../api/v1';
import { MoviesService } from '../services';

@Module({
  imports: [TypeOrmModule.forFeature([MovieRepository, VoteRepository]), AuthModule],
  controllers: [MoviesController],
  providers: [MoviesService],
})
export class MovieModule {}
