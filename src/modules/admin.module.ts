import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminsController } from '../api/v1';
import { AdminsService } from '../services';
import { UserRepository } from '../repository';
import { AuthModule } from './auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository]), AuthModule],
  controllers: [AdminsController],
  providers: [AdminsService],
})
export class AdminModule {}
