export * from './auth.controller';
export * from './admins.controller';
export * from './users.controller';
export * from './movies.controller';
