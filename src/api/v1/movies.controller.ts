import {
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Body, Controller, Get, Param, ParseIntPipe, Post, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { MoviesService } from '../../services';
import {
  CreateMovieDto,
  CreateVoteDto,
  ExceptionDto,
  MovieDto,
  FilterMovieDto,
  VoteDto,
  DetailsMovieDto,
} from '../../dtos';
import { User, Vote } from '../../entities';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../../decorators';

@ApiTags('Movies')
@ApiBearerAuth()
@UseGuards(AuthGuard())
@Controller('movies')
export class MoviesController {
  constructor(
    private movieService: MoviesService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'List movie' })
  @ApiOkResponse({ description: 'Success', type: [MovieDto] })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'Not found', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  listMovies(
    @Query(ValidationPipe) filterMovieDto: FilterMovieDto
  ): Promise<MovieDto[]> {
    return this.movieService.listMovies(filterMovieDto);
  }

  @Get(':id')
  @ApiOperation({ summary: 'List movie' })
  @ApiOkResponse({ description: 'Success', type: DetailsMovieDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'Not found', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  detailsMovie(
    @Param('id', ParseIntPipe) id: number
  ): Promise<DetailsMovieDto> {
    return this.movieService.detailsMovie(id);
  }

  @Post()
  @ApiOperation({ summary: 'Create movie' })
  @ApiCreatedResponse({ description: 'Create', type: MovieDto })
  @ApiConflictResponse({ description: 'Conflict', type: ExceptionDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  createMovie(
    @Body(ValidationPipe) createMovieDto: CreateMovieDto,
    @GetUser() user: User,
  ): Promise<MovieDto> {
    return this.movieService.createMovie(createMovieDto, user);
  }

  @Post('vote')
  @ApiOperation({ summary: 'Create vote' })
  @ApiCreatedResponse({ description: 'Create', type: VoteDto })
  @ApiConflictResponse({ description: 'Conflict', type: ExceptionDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  createVote(
    @Body(ValidationPipe) createVoteDto: CreateVoteDto,
    @GetUser() user: User,
  ): Promise<Vote> {
    return this.movieService.createVote(createVoteDto, user);
  }
}
