import {
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Body, Controller, Delete, Param, ParseIntPipe, Post, Put, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AdminsService } from '../../services';
import { AdminDto, CreateAdminDto, ExceptionDto, UpdateAdminDto } from '../../dtos';
import { User } from '../../entities';

@ApiTags('Administrators')
@Controller('admins')
export class AdminsController {
  constructor(
    private adminService: AdminsService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create administrator' })
  @ApiCreatedResponse({ description: 'Create', type: AdminDto })
  @ApiConflictResponse({ description: 'Conflict', type: ExceptionDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  createAdmin(
    @Body(ValidationPipe) createAdminDto: CreateAdminDto
  ): Promise<User> {
    return this.adminService.createAdmin(createAdminDto);
  }

  @Put(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @ApiOperation({ summary: 'Update administrator' })
  @ApiOkResponse({ description: 'Success', type: Boolean })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'NotFound', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body(ValidationPipe) updateAdminDto: UpdateAdminDto
  ): Promise<boolean> {
    return this.adminService.updateAdmin(id, updateAdminDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @ApiOperation({ summary: 'Delete administrator' })
  @ApiOkResponse({ description: 'Success', type: Boolean })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'NotFound', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  deleteUser(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<boolean> {
    return this.adminService.deleteAdmin(id);
  }
}
