import {
  ApiBearerAuth,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Body, Controller, Delete, Param, ParseIntPipe, Post, Put, UseGuards, ValidationPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from '../../services';
import { CreateUserDto, ExceptionDto, UpdateUserDto, UserDto } from '../../dtos';
import { User } from '../../entities';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(
    private userService: UsersService,
  ) {}

  @Post()
  @ApiOperation({ summary: 'Create user' })
  @ApiCreatedResponse({ description: 'Create', type: UserDto })
  @ApiConflictResponse({ description: 'Conflict', type: ExceptionDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  createUser(
    @Body(ValidationPipe) createUserDto: CreateUserDto
  ): Promise<User> {
    return this.userService.createUser(createUserDto);
  }

  @Put(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @ApiOperation({ summary: 'Update user' })
  @ApiOkResponse({ description: 'Success', type: Boolean })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'NotFound', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body(ValidationPipe) updateUserDto: UpdateUserDto
  ): Promise<boolean> {
    return this.userService.updateUser(id, updateUserDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard())
  @ApiOperation({ summary: 'Delete user' })
  @ApiOkResponse({ description: 'Success', type: Boolean })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiNotFoundResponse({ description: 'NotFound', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  deleteUser(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<boolean> {
    return this.userService.deleteUser(id);
  }
}
