import { Body, Controller, HttpCode, HttpStatus, Post, ValidationPipe } from '@nestjs/common';
import {
  ApiConflictResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { AuthService } from '../../services';
import { AccessTokenDto, CredentialAuthDto, ExceptionDto } from '../../dtos';
import { AccessTokenInterface } from '../../interfaces';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService
  ) {}

  @Post('sign-in')
  @ApiOperation({ summary: 'User sign in' })
  @ApiOkResponse({ description: 'Success', type: AccessTokenDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized', type: ExceptionDto })
  @ApiInternalServerErrorResponse({ description: 'Internal server error', type: ExceptionDto })
  @HttpCode(HttpStatus.OK)
  signIn(
    @Body(ValidationPipe) credentialAuthDto: CredentialAuthDto
  ): Promise<AccessTokenInterface> {
    return this.authService.signIn(credentialAuthDto);
  }
}
