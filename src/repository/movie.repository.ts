import { EntityRepository, QueryBuilder, Repository } from 'typeorm';
import { Movie } from '../entities';
import { CreateMovieDto, FilterMovieDto } from '../dtos';
import { InternalServerErrorException } from '@nestjs/common';

@EntityRepository(Movie)
export class MovieRepository extends Repository<Movie> {
  async findMovies({ search }: FilterMovieDto): Promise<Movie[]> {
    try {
      const query =  this.createQueryBuilder('movies');

      if (search) {
        query.where('movies.name like :search', { search: `%${ search }%` })
          .orWhere('movies.genre like :search', { search: `%${ search }%` })
          .orWhere('movies.director like :search', { search: `%${ search }%` })
          .orWhere('movies.actors like :search', { search: `%${ search }%` });
      }

      return await query.getMany();
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }

  async findMovieById(id: number): Promise<Movie> {
    try {
      return await this.findOne(id);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }

  async createMovie(createMovieDto: CreateMovieDto): Promise<Movie> {
    try {
      const movie = this.create({
        ...createMovieDto,
        genre: createMovieDto.genre.join(','),
        director: createMovieDto.director.join(','),
        actors: createMovieDto.actors.join(','),
      });

      return await this.save(movie);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }
}
