import { EntityRepository, Repository } from 'typeorm';
import { User } from '../entities';
import { CredentialAuthDto } from '../dtos';

@EntityRepository(User)
export class AuthRepository extends Repository<User> {
  async signIn(credentialAuthDto: CredentialAuthDto): Promise<User> {
    const { username, password } = credentialAuthDto;

    return await this.findOne({ username }, {
      select: ['id', 'username', 'password', 'profile'],
    })
      .then(async user => {
        if (await user?.passwordValidade(password)) {
          const { id, username, profile } = user;
          return { id, username, profile } as User;
        }
        return undefined;
      });
  }
}
