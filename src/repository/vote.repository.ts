import { EntityRepository, Repository } from 'typeorm';
import { Movie, User, Vote } from '../entities';
import { CreateVoteDto } from '../dtos';
import { InternalServerErrorException } from '@nestjs/common';

@EntityRepository(Vote)
export class VoteRepository extends Repository<Vote> {
  async createVote(createVoteDto: CreateVoteDto, movie: Movie, user: User): Promise<Vote> {
    try {
      const vote = this.create({
        ...createVoteDto,
        movie,
        user,
      });

      return await this.save(vote);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }

  async getAvgRating(id: number) {
    try {
      return await this.createQueryBuilder('votes')
        .select('TRUNCATE(AVG(votes.rating), 1)', 'avgRating')
        .where('votes.movie_id = :id', { id })
        .getRawOne();
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }
}
