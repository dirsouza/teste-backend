import { ConflictException, InternalServerErrorException } from '@nestjs/common';
import { DeleteResult, EntityRepository, Repository, UpdateResult } from 'typeorm';
import { CreateAdminDto, CreateUserDto, CredentialAuthDto, UpdateAdminDto, UpdateUserDto } from '../dtos';
import { User } from '../entities';
import { Profile } from '../enums';
import { BcryptUtils } from '../utils';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async findUserById(id: number, profile: Profile = Profile.USER): Promise<User> {
    try {
      return await this.findOne({
        where: { id, profile },
      });
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }

  async createUser(
    createUserDto: CreateUserDto | CreateAdminDto | CredentialAuthDto,
    profile: Profile = Profile.USER
  ): Promise<User> {
    try {
      const user = this.create({
        ...createUserDto,
        profile,
        password: await BcryptUtils.hashPassword(createUserDto.password),
      });

      return await this.save(user);
    } catch (e) {
      if (e.errno === 1062) {
        throw new ConflictException('Username already exists.');
      }

      throw new InternalServerErrorException(e.message);
    }
  }

  async updateUser(user: User, updateUserDto: UpdateUserDto | UpdateAdminDto): Promise<UpdateResult> {
    try {
      if (updateUserDto.password) {
        updateUserDto = {
          ...updateUserDto,
          password: await BcryptUtils.hashPassword(updateUserDto.password),
        };
      }

      return await this.update(user.id, updateUserDto);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }

  async deleteUser(user: User): Promise<DeleteResult> {
    try {
      return await this.softDelete(user.id);
    } catch (e) {
      throw new InternalServerErrorException(e.message);
    }
  }
}
