export interface JwtPayloadInterface {
  id: number;
  username: string;
  profile: string;
}

export interface AccessTokenInterface {
  token: string;
  expires_in?: string | number;
  type?: string;
}
